package com.google.android.exoplayer.audio;

public class NativeAudioInterface {
	
	static { System.loadLibrary("VRSE"); }
	public static native int nativeGetAudioPosition();
	public static native void nativeSetAudioPlayback(boolean play);
	public static native void nativeLoadAudio(String[] pathsArray);
	public static native void nativeCycleAudio(int dir);
	public static native void nativeSetViewingAngle(float angle);
	public static native void nativeSeekAudio(int position);
	public static native void nativeSetMute(boolean mute);
	public static native void nativeSetBufferSize(int size);
	public static native void nativeSetSampleRate(int rate);
}
