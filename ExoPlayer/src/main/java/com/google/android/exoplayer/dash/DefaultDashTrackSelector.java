package com.google.android.exoplayer.dash;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaCodecInfo;
import android.os.Build;
import android.util.Log;

import com.google.android.exoplayer.MediaCodecUtil;
import com.google.android.exoplayer.Within;
import com.google.android.exoplayer.chunk.Format;
import com.google.android.exoplayer.chunk.FormatWrapper;
import com.google.android.exoplayer.dash.mpd.AdaptationSet;
import com.google.android.exoplayer.dash.mpd.MediaPresentationDescription;
import com.google.android.exoplayer.dash.mpd.Period;
import com.google.android.exoplayer.util.MimeTypes;
import com.google.android.exoplayer.util.Util;
import com.google.android.exoplayer.util.VideoTester;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 7/26/16.
 */
public class DefaultDashTrackSelector implements DashTrackSelector {

    private final int adaptationSetType;

    private final Context context;
    private final boolean filterVideoRepresentations;
    private final boolean filterProtectedHdContent;

    public String platform;

    /**
     * @param context A context. May be null if {@code filterVideoRepresentations == false}.
     * @param filterVideoRepresentations Whether video representations should be filtered according to
     *     the capabilities of the device. It is strongly recommended to set this to {@code true},
     *     unless the application has already verified that all representations are playable.
     * @param filterProtectedHdContent Whether video representations that are both drm protected and
     *     high definition should be filtered when tracks are built. If
     *     {@code filterVideoRepresentations == false} then this parameter is ignored.
     */
    public static DefaultDashTrackSelector newVideoInstance(Context context,
                                                       boolean filterVideoRepresentations, boolean filterProtectedHdContent) {
        return new DefaultDashTrackSelector(AdaptationSet.TYPE_VIDEO, context,
                filterVideoRepresentations, filterProtectedHdContent);
    }

    public static DefaultDashTrackSelector newAudioInstance() {
        return new DefaultDashTrackSelector(AdaptationSet.TYPE_AUDIO, null, false, false);
    }

    public static DefaultDashTrackSelector newTextInstance() {
        return new DefaultDashTrackSelector(AdaptationSet.TYPE_TEXT, null, false, false);
    }

    private DefaultDashTrackSelector(int adaptationSetType, Context context,
                                boolean filterVideoRepresentations, boolean filterProtectedHdContent) {
        this.adaptationSetType = adaptationSetType;
        this.context = context;
        this.filterVideoRepresentations = filterVideoRepresentations;
        this.filterProtectedHdContent = filterProtectedHdContent;
    }

    @Override
    public void selectTracks(MediaPresentationDescription manifest, int periodIndex, Output output)
            throws IOException {
        Period period = manifest.getPeriod(periodIndex);
        for (int i = 0; i < period.adaptationSets.size(); i++) {
            AdaptationSet adaptationSet = period.adaptationSets.get(i);
            if (adaptationSet.type == adaptationSetType) {
                if (adaptationSetType == AdaptationSet.TYPE_VIDEO) {
                    int[] representations;
                    if (filterVideoRepresentations) {
                        representations = selectVideoFormats(adaptationSet.representations, null,
                                filterProtectedHdContent && adaptationSet.hasContentProtection());
                    } else {
                        representations = Util.firstIntegersArray(adaptationSet.representations.size());
                    }
                    int representationCount = representations.length;
                    if (representationCount > 1) {
                        output.adaptiveTrack(manifest, periodIndex, i, representations);
                    }
                    for (int j = 0; j < representationCount; j++) {
                        output.fixedTrack(manifest, periodIndex, i, representations[j]);
                    }
                } else {
                    for (int j = 0; j < adaptationSet.representations.size(); j++) {
                        output.fixedTrack(manifest, periodIndex, i, j);
                    }
                }
            }
        }
    }

    /**
     * Chooses a suitable subset from a number of video formats.
     *
     * @param formatWrappers Wrapped formats from which to select.
     * @param allowedContainerMimeTypes An array of allowed container mime types. Null allows all
     *     mime types.
     * @param filterHdFormats True to filter HD formats. False otherwise.
     * @return An array holding the indices of the selected formats.
     */
    public int[] selectVideoFormats(List<? extends FormatWrapper> formatWrappers,
                                           String[] allowedContainerMimeTypes, boolean filterHdFormats) throws MediaCodecUtil.DecoderQueryException {

        platform = Within.getInstance().getVrPlatform();
        Log.d("selectVideoFormats", "VR Platform="+platform);

        ArrayList<Integer> selectedIndexList = new ArrayList<>();
        int formatWrapperCount = formatWrappers.size();
        for (int i = 0; i < formatWrapperCount; i++) {
            Format format = formatWrappers.get(i).getFormat();
            switch(platform) {
                case "GEARVR":
                    selectedIndexList.add(i);
                    break;
                case "CARDBOARD":
                    if (isFormatPlayable(format, allowedContainerMimeTypes, filterHdFormats)) {
                        selectedIndexList.add(i);
                    }
                    break;
                case "DAYDREAM":
                    selectedIndexList.add(i);
                    break;
                default:
                    selectedIndexList.add(i);
                    return null;
            }
        }

        Log.d("selectVideoFormats", "selected track size="+selectedIndexList.size());
        return Util.toArray(selectedIndexList);
    }

    /**
     * Determines whether an individual format is playable, given an array of allowed container types,
     * whether HD formats should be filtered and a maximum decodable frame size in pixels.
     */
    private boolean isFormatPlayable(Format format, String[] allowedContainerMimeTypes,
                                     boolean filterHdFormats) throws MediaCodecUtil.DecoderQueryException {
        if (allowedContainerMimeTypes != null
                && !Util.contains(allowedContainerMimeTypes, format.mimeType)) {
            // Filtering format based on its container mime type.
            return false;
        }
        if (filterHdFormats && (format.width >= 1280 || format.height >= 720)) {
            // Filtering format because it's HD.
            return false;
        }
        if (format.width > 0 && format.height > 0) {
            if (Util.SDK_INT >= 21) {
                String videoMediaMimeType = MimeTypes.getVideoMediaMimeType(format.codecs);

                if (MimeTypes.VIDEO_UNKNOWN.equals(videoMediaMimeType)) {
                    // Assume the video is H.264.
                    videoMediaMimeType = MimeTypes.VIDEO_H264;
                }

                if (format.frameRate > 0) {
                    // Primary check to see if device advertises support for size and framerate
                    boolean isSizeAndRateSupported = MediaCodecUtil.isSizeAndRateSupportedV21(videoMediaMimeType, false, format.width,
                           format.height, format.frameRate);

                    // Secondary check for higher res videos that may actually be playable on device
                    boolean is4KCapable = format.height >= 1440 && is4KCapable(context);

                    // video is playable if one of these are true
                    return (isSizeAndRateSupported || is4KCapable);
                } else {
                    return MediaCodecUtil.isSizeSupportedV21(videoMediaMimeType, false, format.width,
                            format.height);
                }
            }
            // Assume the video is H.264.
            if (format.width * format.height > MediaCodecUtil.maxH264DecodableFrameSize()) {
                // Filtering format because it exceeds the maximum decodable frame size.
                return false;
            }
        }
        return true;
    }


    private static final String KEY_PREFS = "vrsePrefs";
    private static final String KEY_4K_CAPABLE = "4K_Capable";

    public static boolean is4KCapable(final Context context) {
        boolean isCapable;
        //manual override
        if (Build.DEVICE.equals("g3")){
            return true;
        } else if (Build.DEVICE.contains("htc_m8")){
            return false;
        } else {
            int capable = context.getSharedPreferences(KEY_PREFS, 0).getInt(KEY_4K_CAPABLE, -1);

            if (capable == -1){
                if(Build.VERSION.SDK_INT >= 21){
                    //check if supports with lollipop code
                    MediaCodecInfo mediaCodecInfo = VideoTester.selectCodec(VideoTester.getMimetype());

                    try {
                        isCapable = mediaCodecInfo.getCapabilitiesForType(VideoTester.getMimetype()).getVideoCapabilities().areSizeAndRateSupported(3840, 2160, 30); //UHD1
                    }catch(NullPointerException e){
                        isCapable = false;
                    }

                    context.getSharedPreferences(KEY_PREFS, 0).edit().putInt(KEY_4K_CAPABLE, isCapable ? 1 : 0).apply();
                    return isCapable;
                    //2880, 1920 30 HHD
                }
                else {
                    isCapable = VideoTester.test4KVideo(context);
                    context.getSharedPreferences(KEY_PREFS, 0).edit().putInt(KEY_4K_CAPABLE, isCapable ? 1 : 0).apply();
                    return isCapable;
                }
            }
            return capable == 1;
        }
    }


}
