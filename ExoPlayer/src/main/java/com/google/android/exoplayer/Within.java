package com.google.android.exoplayer;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import java.io.File;


/**
 * Created by jacob on 9/30/2016.
 */
public class Within extends Activity {
    private static Within instance;
    private Context context;

    private ExoPlayerImpl player;
    private String[] nativeAudioSource_filenames;
    private int nativeAudioSampleRate;

    private String vrPlatform;

    public static Within getInstance(Context context) {
        if (instance == null) {
            instance = new Within();
            instance.context = context;
        }
        return instance;
    }

    public static Within getInstance() {
        if (instance == null) instance = new Within();
        return instance;
    }


    public void setPlayer(ExoPlayerImpl player) { this.player = player; }


    /**
     * Set VR Platform to determine which tracks should be selected
     */
    public void setVrPlatform(String platform) { vrPlatform = platform; }
    public String getVrPlatform() { return vrPlatform; }


    /**
     * Set the paths to external audio
     */
    public void setNativeAudioSource(String source, int sampleRate) {
        nativeAudioSource_filenames = source.split(",");
        if (nativeAudioSource_filenames.length != 4) nativeAudioSource_filenames = null;
        nativeAudioSampleRate = sampleRate;
    }


    /**
     * Send the new viewing angle to the exoplayer/NativeAudioManager instance
     */
    public void updateViewingAngle(float angle) {
        if (player != null) player.updateViewingAngle(angle);
    }

    public String[] getNativeAudioSource() {
        return nativeAudioSource_filenames;
    }

    public int getNativeAudioSampleRate() { return nativeAudioSampleRate; }

    public void playAudio() {
        if (player != null) {
            player.playAudio();
        }
    }

    public void pauseAudio() {
        if (player != null) {
            player.pauseAudio();
        }
    }

    public void setContext(Context unityContext) {
        context = unityContext;
    }

    public Context getContext() {
        return context;
    }

}
