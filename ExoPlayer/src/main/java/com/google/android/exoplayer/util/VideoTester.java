/*
 * Copyright (c) 2015. Shaking Earth Digital
 */

package com.google.android.exoplayer.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaFormat;
import android.media.MediaMetadataRetriever;
import android.os.Build;

import java.io.IOException;

/**
 * Created by Jens Zalzala on 1/21/15.
 *
 * Test video playback capability
 */
public class VideoTester {

    public static boolean test4KVideo(Context context){

        //try to extract thumbnail. if it fails, device is not capable.
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try {
            final AssetFileDescriptor afd=context.getAssets().openFd("video4k_test.mp4");
            retriever.setDataSource(afd.getFileDescriptor(),afd.getStartOffset(),afd.getLength());

            Bitmap frameAtTime = retriever.getFrameAtTime();

            if (frameAtTime != null){
                retriever.release();
                frameAtTime.recycle();
                return true;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static String getMimetype(){
        return MediaFormat.MIMETYPE_VIDEO_AVC;
    }

    public static MediaCodecInfo selectCodec(String mimeType) {
        int numCodecs = MediaCodecList.getCodecCount();
        for (int i = 0; i < numCodecs; i++) {
            MediaCodecInfo codecInfo = MediaCodecList.getCodecInfoAt(i);

            if (!codecInfo.isEncoder()) {
                continue;
            }

            String[] types = codecInfo.getSupportedTypes();
            for (int j = 0; j < types.length; j++) {
                if (types[j].equalsIgnoreCase(mimeType)) {
                    return codecInfo;
                }
            }
        }
        return null;
    }

}
