package com.google.android.exoplayer.audio;

import android.content.Context;
import android.media.AudioManager;
import android.os.Handler;
import android.util.Log;

import com.google.android.exoplayer.ExoPlayerImpl;
import com.google.android.exoplayer.Within;

import java.io.File;


public class NativeAudioManager implements AudioCapabilitiesReceiver.Listener {

	Context context;
    ExoPlayerImpl player;
    VideoSyncRunnable mVideoSyncRunnable = new VideoSyncRunnable();
	AudioManager mAudioManager;
    AudioCapabilitiesReceiver audioCapabilitiesReceiver;
    boolean audioBehind;
    String[] absolutePaths;
    int audioSampleRate;

	AudioManager.OnAudioFocusChangeListener mAudioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() {
        @Override
        public void onAudioFocusChange(int focusChange) {
            //Log.d("mAudioFocusChangeListen", "audio focus changed " + focusChange);
        }
    };
    private boolean stopped = false;


    public NativeAudioManager(ExoPlayerImpl player, String[] absolutePaths, int audioSampleRate) {
        this.player = player;
    	this.context = Within.getInstance().getContext();
        this.absolutePaths = absolutePaths;
        this.audioSampleRate = audioSampleRate;
    	init();
    }


    public void setVideoPlayer(ExoPlayerImpl player) {
        this.player = player;
    }

    public void init() {
    	Log.d("NativeAudioManager", "init");
    	AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        
        if (audioManager.isBluetoothA2dpOn()) {
            NativeAudioInterface.nativeSetBufferSize(16384);
        } else {
            int framesPerBuffer = Integer.parseInt(audioManager.getProperty(AudioManager.PROPERTY_OUTPUT_FRAMES_PER_BUFFER));
            int channels = 2;//stereo
            int size = 4;//size of char

            int bufferSize = framesPerBuffer * channels * size;
            bufferSize = Math.max(4096, bufferSize);

            NativeAudioInterface.nativeSetBufferSize(bufferSize);
        }

        audioCapabilitiesReceiver = new AudioCapabilitiesReceiver(context, this);
        audioCapabilitiesReceiver.register();
        audioBehind = false;

        NativeAudioInterface.nativeSetSampleRate(audioSampleRate);    // get audio sample rate

        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
    }

    public void onDestroy() {
        audioCapabilitiesReceiver.unregister();
    }
    public void onResume() {

    }

    public void loadAudio() {
        NativeAudioInterface.nativeLoadAudio(absolutePaths);
    }

    // FIXME after testing
    public static String[] getPathsFromFileName(String[] filenames) {
        try {
            File cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"within_audio_files");
            if (!cacheDir.exists()) return null;

            String[] absolutePaths = new String[4];

            for (int i=0; i < filenames.length; i++) {
                File f = new File(cacheDir, filenames[i]);
                if (f.exists()) {
                    absolutePaths[i] = f.getAbsolutePath();
                    Log.d("loadAudio", absolutePaths[i]);
                    Log.d("loadAudio", filenames[i]+" added to streaming paths!");
                } else {
                    Log.d("loadAudio", filenames[i]+" doesn't exist!");
                }
            }

            return absolutePaths;
        } catch (Exception e) {
            return null;
        }
    }

	private void setVolumeControlStream(int streamMusic) {
	}
	

	float mInitialHeadTransformAngle;
	float mHeadTransformAngle;

	public void updateViewingAngle(float angle) {
        mHeadTransformAngle = (float) (Math.toRadians(angle) - Math.PI);
        float audioAngle = (float) ((mHeadTransformAngle - mInitialHeadTransformAngle + (2 * Math.PI)) % (2 * Math.PI));
        NativeAudioInterface.nativeSetViewingAngle(audioAngle);
	}
	
	public void pauseAudio() {
        if (mAudioManager != null) mAudioManager.abandonAudioFocus(mAudioFocusChangeListener);
		NativeAudioInterface.nativeSetAudioPlayback(false);
        mVideoSyncRunnable.killMyself();
	}

    public void stopAudio() {
        if (stopped) return;
        stopped = true;
        pauseAudio();
        NativeAudioInterface.nativeSeekAudio(0);
        mVideoSyncRunnable.killMyself();
    }

    public void seekAudio(int seekPosition) {
        NativeAudioInterface.nativeSeekAudio(seekPosition);
    }

	public void playAudio() {
        stopped = false;
        int result = mAudioManager.requestAudioFocus(
                mAudioFocusChangeListener,
                // Use the music stream.
                AudioManager.STREAM_MUSIC,
                // Request permanent focus.
                AudioManager.AUDIOFOCUS_GAIN);

        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            //Log.d("onResume", "audio focus granted");
        } else {
            //Log.w("onResume", "audio focus not granted");
        }
		NativeAudioInterface.nativeSetAudioPlayback(true);
        mVideoSyncRunnable.start(500);
	}


    public void syncAudioVideo() {
        int audioPosition = NativeAudioInterface.nativeGetAudioPosition();
        int videoPosition;
        try {
            videoPosition = (int) player.getCurrentPosition();
        } catch (IllegalStateException e) {
            mVideoSyncRunnable.killMyself();
            return;
        }
        Log.d("syncAudioVideo","video position " + videoPosition);
        int difference = audioPosition - videoPosition;
        Log.d("syncAudioVideo","video audio position difference = " + difference);
        if (difference < -60) {
            //video is ahead of audio
            Log.d("syncAudioVideo","pause video");

            if (player != null) player.pauseVideo();

            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (player != null) player.playVideo();
                    } catch (IllegalStateException e) {
                        //Log.e(e.getMessage(), "syncAudioVideo : run");
                    }
                }
            }, -difference);
        } else if (difference > 60) {
            //audio is ahead of video
            Log.d("syncAudioVideo", "pause audio");
            pauseAudio();
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!stopped){
                        playAudio();
                    }
                }
            }, difference);
        }
    }

	
	Handler mHandler = new Handler();
	protected boolean mSyncRecheck = false;
	protected static final int VIDEO_SYNC_DELAY = 10000;

    @Override
    public void onAudioCapabilitiesChanged(AudioCapabilities audioCapabilities) {

    }

    class VideoSyncRunnable implements Runnable {
        private boolean killMe = true;

        @Override
        public void run() {
            synchronized (this) {
                if (!killMe) {
                    Log.d("VideoSyncRunnable", "run : mSyncRecheck="+mSyncRecheck);
                    syncAudioVideo();
                    mHandler.postDelayed(this, mSyncRecheck ? 1000 : VIDEO_SYNC_DELAY);
                }
            }
        }

        public void killMyself() {
            synchronized (this) {
                Log.d("VideoSyncRunnable", "killMyself");
                killMe = true;
                mHandler.removeCallbacks(this);
            }
        }

        public void start(int delay) {
            synchronized (this) {
                //only start if previously killed
                if (killMe) {
                    Log.d("VideoSyncRunnable", "start");
                    killMe = false;
                    mHandler.postDelayed(this, delay);
                }
            }
        }
    }
	
	
}
