package com.shakingearthdigital.contentdownloadplugin;

import android.content.Context;
import android.util.Log;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shakingearthdigital.contentdownloadplugin.models.within.Camera;
import com.shakingearthdigital.contentdownloadplugin.models.within.ContentImage;
import com.shakingearthdigital.contentdownloadplugin.models.within.ContentLink;
import com.shakingearthdigital.contentdownloadplugin.models.within.ContentLocal;
import com.shakingearthdigital.contentdownloadplugin.utils.ContentUtil;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by jacob on 9/16/2016.
 */
public class ContentWriter {
    public Context context;
    public String path;
    public static ContentWriter instance;

    public ContentWriter ContentWriter(Context context) {
        if (instance == null) {
            instance = new ContentWriter();
            instance.context = context;
        }
        return instance;
    }

    public static boolean writeJSON(Context context, String serializedObject, String path, boolean useExternalStorage) {
        Log.d("writeJson", "save path="+path);
        ObjectMapper mapper = new ObjectMapper();

        try {
            ContentLink content = mapper.readValue(serializedObject, ContentLink.class);
            ContentLocal contentLocal = ContentUtil.createContentLocal(context, content, path);
            ContentUtil.with(context).writeContentLocal(context, contentLocal, path, useExternalStorage);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return true;
    }

    public static ContentLocal createContentLocal(int contentId) {
        return new ContentLocal();
    }

    public static String createSerializedObject(ContentLink contentLink) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(contentLink);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * A test contentlink to demo json writing. ContentLink should already be available in C#
     */
    public static ContentLink testContentLink() {
        ContentLink contentLink = new ContentLink();
        contentLink.setId(71);
        contentLink.setTitle("SNL 40: Seinfeld Q&A");
        contentLink.setAuthor("By SNL & Chris Milk");
        contentLink.setDescription("Join the live audience as SNL celebrates 40 years in a star-studded special. Jerry Seinfeld hosts a Q&A with surprise guests.");

        // images
        ArrayList<ContentImage> contentImages = new ArrayList();
        contentImages.add(new ContentImage("gear_thumb_main", "http://cdn1.with.in/71-SNL/images/gearvr-seinfeld.png"));
        contentImages.add(new ContentImage("ac_thumb_main", "http://cdn1.with.in/71-SNL/images/Seinfeld_1080x569.jpg"));
        contentImages.add(new ContentImage("ac_detail_main", "http://cdn1.with.in/71-SNL/images/Seinfeld_NoText_1080x569.jpg"));
        contentLink.setImages(contentImages);

        // camera
        ArrayList<Camera> cameras = new ArrayList();
        Camera camera = new Camera();
        camera.setVideo("http://cdn1.with.in/71-SNL/video/snl-uhd1-sm-q22-30fps-pbaseline-l4.2.mp4");
        camera.setVideoFormat("stereo_top_bottom");
        camera.setAudio(new String[] {"http://cdn1.with.in/71-SNL/audio/SNL_Seinfeld_v7_000.mp3",
                "http://cdn1.with.in/71-SNL/audio/SNL_Seinfeld_v7_090.mp3",
                "http://cdn1.with.in/71-SNL/audio/SNL_Seinfeld_v7_180.mp3",
                "http://cdn1.with.in/71-SNL/audio/SNL_Seinfeld_v7_270.mp3"});

        contentLink.setCameras(cameras);

        return contentLink;
    }
}
