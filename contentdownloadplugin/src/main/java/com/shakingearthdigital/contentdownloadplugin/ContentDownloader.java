package com.shakingearthdigital.contentdownloadplugin;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;

import com.shakingearthdigital.contentdownloadplugin.managers.ContentManager;
import com.shakingearthdigital.contentdownloadplugin.models.DownloadItem;
import com.shakingearthdigital.contentdownloadplugin.utils.SELogUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by jacob on 9/9/2016.
 */
public class ContentDownloader implements DownloadService.Listener {
    SELogUtil log = new SELogUtil("ContentDownloader");

    public DownloadService mDownloadService;
    public DownloadService.Listener listener;
    public boolean serviceIsBound;
    public Context context;
    public static ContentDownloader instance;

    public static ContentDownloader getInstance(Context context) {
        if (instance == null) {
            instance = new ContentDownloader(context);
            DownloadService.setListener(instance);
        }
        return instance;
    }

    public ContentDownloader(Context context) {
        this.context = context;
        connectDownloadService();
    }

    public void setListener(DownloadService.Listener listener) {
        this.listener = listener;
    }

    public void downloadContent(int contentId, String [] urls) {
        DownloadService.downloadContentService(context, contentId, new ArrayList<String>(Arrays.asList(urls)));
        connectDownloadService();
    }

    /**
     * used in conjunction with DownloadItem.Builder to apply all chosen parameters at once.
     */
    public void downloadContent(DownloadItem item) {
        DownloadService.downloadContentService(context, item);
        connectDownloadService();
    }

    public void cancelDownload(int contentId) {
        DownloadService.cancelDownloadService(context, contentId);
        connectDownloadService();
    }

    public int getServiceState() {
        return DownloadService.getServiceState().GetValue();
    }

    public int getContentState(int contentId) {
        if (mDownloadService != null) {
            DownloadItem item = mDownloadService.getDownloadItem(contentId);
            if (null == item) return DownloadItem.STATE.NOT_SET_TO_DOWNLOAD.GetValue();
            else return item.state.GetValue();
        }
        return DownloadItem.STATE.NOT_SET_TO_DOWNLOAD.GetValue();
    }

    public boolean isDownloaded(int contentId) {
        return ContentManager.isDownloaded(context, contentId);
    }

    public void deleteContent(int contentId) {
        if (ContentManager.deleteContent(context, contentId)) onContentDeleted(contentId);
    }

    public String getLocalVideoPath(int contentId, String videoUrl) {
        return ContentManager.getLocalVideoPath(context, contentId, videoUrl);
    }

    public String getLocalVideoUri(int contentId, String videoUrl) {
        return Uri.fromFile(new File(ContentManager.getLocalVideoPath(context, contentId, videoUrl))).toString();
        // return Uri.fromFile(new File(ContentManager.getLocalVideoPath(context, contentId, videoUrl)));
    }

    /**
     * Returns the absolute path for already downloaded audio.
     * Used for playing streaming videos.
     */
    public String[] getLocalAudioPaths(int contentId, String... audioLinks) {
        return ContentManager.getLocalAudioPaths(context, contentId, audioLinks);
    }

    public boolean isAudioDownloadComplete(int contentId, String... audioLinks) {
        return ContentManager.isAudioDownloadComplete(context, contentId, audioLinks);
    }

    /**
     * Returns the download progress of an item.
     * Progress is between 0-1
     */
    public float getProgress(int contentId) {
        if (mDownloadService != null) {
            return mDownloadService.getProgress(contentId);
        }
        return 0f;
    }

    /**
     * if true, set device to only download over wi-fi.
     */
    public void setDownloadOverWifiOnly(boolean value) {
        ContentManager.setWifiDownloadOnlyPref(context, value);
    }

    public boolean getDownloadOverWifiOnly() {
        return ContentManager.getWifiDownloadOnlyPref(context);
    }

    /**
     * Download to removable storage if available
     */
    public void useExternalStorage(boolean value) {
        ContentManager.setDownloadPref(context, value);
    }

    public boolean getUseExternalStorage() {
        return ContentManager.getDownloadPref(context);
    }

    /**
     * true if device has accessible external storage (e.g. sd card)
     */
    public boolean hasExternalStorage() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /**
     * set the folder that will be created inside the root package directory.
     * If not set, the default directory is 'Content'
     * @param dir
     */
    public void setDirectoryName(String dir) {
        ContentManager.setContentDir(context, dir);
    }

    /**
     * Prefix for folders containing a download item. Ex: "item_71" where "item" is the prefix.
     */
    public void setDirPrefix(String prefix) {
        ContentManager.setContentDirChildPrefix(context, prefix);
    }

    /**
     * Returns the contentId of the currently downloading item.
     * Returns -1 if there is nothing downloading.
     */
    public int getActiveDownload() {
        return mDownloadService.getActiveDownload();
    }

    /**
     * Returns the length of downloads in queue. (Including the active download)
     */
    public int getDownloadCount() {
        return mDownloadService.getDownloadCount();
    }

    public int getPositionInQueue(int contentId) {
        return mDownloadService.getPositionInQueue(contentId);
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            mDownloadService = ((DownloadService.DownloadServiceBinder)service).getService();
            log.d("onServiceConnected");
        }

        public void onServiceDisconnected(ComponentName className) {
            // This should never happen
            mDownloadService = null;
            log.e("onServiceDisconnected : Download service connection lost");
        }
    };


    private void connectDownloadService() {
        log.d("connectDownloadService");
        serviceIsBound = context.bindService(new Intent(context, DownloadService.class), mConnection, Context.BIND_AUTO_CREATE);

        if (serviceIsBound){
            log.d("mIsBound");
        }
        else {
            log.d("mIsBound == false");
        }
    }

    /**
     * Unbinds the download service.
     */
    public void disconnectDownloadService() {
        if (serviceIsBound) {
            try {
                context.unbindService(mConnection);
            } catch (IllegalArgumentException e){
                log.e(e.getMessage());
            } finally {
                serviceIsBound = false;
            }
        }
    }

    @Override
    public void onServiceStateChanged(int state) {
        if (listener != null) listener.onServiceStateChanged(state);
    }

    @Override
    public void onDownloadAdded(int contentId, int contentState) {
        if (listener != null) listener.onDownloadAdded(contentId, contentState);
    }

    @Override
    public void onProgressChanged(int contentId, float progress) {
        // if (listener != null) listener.onProgressChanged(contentId, progress);
    }

    @Override
    public void onDownloadCompleted(int contentId, boolean onlyAudio) {
        if (listener != null) listener.onDownloadCompleted(contentId, onlyAudio);
    }

    @Override
    public void onDownloadCanceled(int contentId) {
        if (listener != null) listener.onDownloadCanceled(contentId);
    }

    @Override
    public void onDownloadFailed(int contentId, String error) {
        if (listener != null) listener.onDownloadFailed(contentId, error);
    }

    @Override
    public void onContentDeleted(int contentId) {
        if (listener != null) listener.onContentDeleted(contentId);
    }
}
