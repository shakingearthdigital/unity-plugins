package com.shakingearthdigital.contentdownloadplugin.models;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.shakingearthdigital.contentdownloadplugin.CancelDownloadService;
import com.shakingearthdigital.contentdownloadplugin.DownloadService;
import com.shakingearthdigital.contentdownloadplugin.R;
import com.shakingearthdigital.contentdownloadplugin.managers.ContentManager;
import com.shakingearthdigital.contentdownloadplugin.utils.BaseUtil;
import com.shakingearthdigital.contentdownloadplugin.ContentWriter;

import java.io.File;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by jacob on 9/8/2016.
 */
public class DownloadItem implements Serializable {
    final static String EXTRA_NOTIFICATION = "notificationIntent";

    // Minimum required values to form an item ready for downloading
    public final int contentId;
    public volatile ArrayList<String> urls;

    // notification attributes
    public String notify_Title;
    public String notify_Image;
    private String notify_ResultIntentUri;
    public NotificationCompat.Builder notify_Builder;

    // download progress tracking
    private long downloaded;
    private long size;
    private int percent;
    private String json;

    public boolean writeJson = false;

    public STATE state;
    public enum STATE {
        DOWNLOADING(0),
        QUEUED(1),
        CANCELED(2),
        ERROR(3),
        NOT_SET_TO_DOWNLOAD(4);

        private int iValue;
        STATE(int i) { iValue = i; }
        public int GetValue() {  return iValue; }
    }

    public DownloadItem(int contentId, ArrayList<String> urls) {
        this.contentId = contentId;
        this.urls = urls;
        this.state = STATE.QUEUED;
    }

    public DownloadItem(int contentId, String[] urls) {
        this.contentId = contentId;
        this.urls = new ArrayList<>(Arrays.asList(urls));
        this.state = STATE.QUEUED;
    }

    public void setUrls(String [] urls) {
        this.urls = new ArrayList<>(Arrays.asList(urls));
    }
    public void setUrls(ArrayList<String> urls) { this.urls = urls; }

    public void setNotificationInfo(String title, String image) {
        this.notify_Title = title;
        this.notify_Image = image;
    }

    public void setResultIntent(Intent intent) {
        intent.putExtra(EXTRA_NOTIFICATION, contentId);
        this.notify_ResultIntentUri = intent.toUri(Intent.URI_INTENT_SCHEME);
    }

    public void setWriteJson(boolean write, String jsonString) {
        this.writeJson = write;
        this.json = jsonString;
    }

    public void writeJson(Context context) {
        Log.d("writeJson", "external="+ContentManager.getDownloadPref(context));
        ContentWriter.writeJSON(context, json, ContentManager.getContentDir(context, contentId).getPath(), ContentManager.getDownloadPref(context));
    }

    public boolean useNotification() {
        if (notify_Title == null && notify_Image == null && notify_ResultIntentUri == null) return false;
        else return true;
    }

    public void setSize(long downloadSize) {
        this.size = downloadSize;
    }

    public float getProgress() {
        return size == 0 ? 0 : (float) downloaded / size;
    }

    public int getPercent() { return percent; }

    // Returns true if the download percent increased
    public boolean addProgress(long downloaded) {
        this.downloaded += downloaded;
        int newPercent = (int) (getProgress() * 100);

        Log.d("addProgress", "downloaded="+downloaded+" : newPercent="+newPercent);

        if (newPercent > percent) {
            percent = newPercent;
            if (notify_Builder != null) {
                notify_Builder.setProgress(100, percent, false);
            }
            return true;
        }

        return false;
    }

    public Notification getNotification(String message) {
        if (message != null) {
            notify_Builder.setContentText(message);
            return notify_Builder.build();
        }

        if (state == STATE.ERROR) {
            notify_Builder.setContentText("Error");
        }
        else if (DownloadService.getServiceState() == DownloadService.STATE.WAITING) {
            notify_Builder.setContentText("Waiting to download...");
        }
        else {
            notify_Builder.setContentText("Downloading...");
        }

        return notify_Builder.build();
    }

    public Notification getNotification() {
        return getNotification(null);
    }

    public void buildNotification(Context context) {
        notify_Builder = new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_stat_download)
                        .setOngoing(true);

        if (notify_Title != null) notify_Builder.setContentTitle(notify_Title);

        notify_Builder.setProgress(100, 0, false);

        if (notify_ResultIntentUri != null) {
            try {
                Intent resultIntent = Intent.parseUri(notify_ResultIntentUri, Intent.URI_INTENT_SCHEME);
                PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                notify_Builder.setContentIntent(resultPendingIntent);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

        }

        // Set thumbnail image for content
        if(notify_Image != null && new File(notify_Image).exists()) {
            int resize = (int) BaseUtil.dpToPx(100, context);
            Bitmap bitmap = BitmapFactory.decodeFile(notify_Image, null);
            bitmap = Bitmap.createScaledBitmap(bitmap, resize, resize, true);
            notify_Builder.setLargeIcon(bitmap);
        }

        Intent cancelIntent = CancelDownloadService.getLaunchIntent(context, contentId);
        PendingIntent cancelPendingIntent = PendingIntent.getService(context, 0, cancelIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notify_Builder.addAction(R.drawable.ic_download_cancel, "Cancel", cancelPendingIntent);
    }


}
