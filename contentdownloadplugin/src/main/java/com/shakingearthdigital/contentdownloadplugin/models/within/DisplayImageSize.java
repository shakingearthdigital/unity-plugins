package com.shakingearthdigital.contentdownloadplugin.models.within;

/**
 * Created by Brendon on 7/30/2015.
 */
public class DisplayImageSize {
    public int height;
    public int width;
    public DisplayImageSize(int height, int width){
        this.height = height;
        this.width = width;
    }
}
