/*
 * Copyright (c) 2015. Shaking Earth Digital
 */

package com.shakingearthdigital.contentdownloadplugin.models.within;

/**
 * Created by Jens Zalzala on 1/2/15.
 * Video Type Enum
 */
public enum VideoFormat {
    STEREO_TOP_BOTTOM,
    STEREO_LEFT_RIGHT,
    MONO
}
