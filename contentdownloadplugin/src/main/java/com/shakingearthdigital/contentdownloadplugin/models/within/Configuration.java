package com.shakingearthdigital.contentdownloadplugin.models.within;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.shakingearthdigital.contentdownloadplugin.utils.SELogUtil;

public class Configuration {

	private String updateUrl;
	private UIConfig uiConfig;
	private ContentConfig[] contentConfigs;
	private boolean filterExplicitContent = false;
	private int[] excludeContent;
	private String startTime;


	public Configuration (){
	}

	@JsonProperty("startTime")
	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	@JsonProperty("contentConfiguration")
	public ContentConfig[] getContentConfigs() {
		return contentConfigs;
	}

	public void setContentConfigs(ContentConfig[] contentConfigs) {
		this.contentConfigs = contentConfigs;
	}

	@JsonProperty("updateUrl")
	public String getUpdateUrl() {
		return updateUrl;
	}

	public void setUpdateUrl(String updateUrl) {
		this.updateUrl = updateUrl;
	}

	@JsonProperty("uiOptions")
	public UIConfig getUiConfig() {
		return uiConfig;
	}

	public void setUiConfig(UIConfig uiConfig) {
		this.uiConfig = uiConfig;
	}
	@JsonProperty("filterExplicitContent")
	public boolean isFilterExplicitContent() {
		return filterExplicitContent;
	}
	public void setFilterExplicitContent(boolean filterExplicitContent) {
		this.filterExplicitContent = filterExplicitContent;
	}

	@JsonProperty("excludeContent")
	public int[] getExcludeContent() {
		return excludeContent;
	}
	public void setExcludeContent(int[] excludeContent) {
		this.excludeContent = excludeContent;
	}


	@SuppressWarnings("Configuration")
	@JsonAnySetter
	public void handleUnknown(String key, Object value) {
		//unknown properties
		SELogUtil.logW("ContentConfig handleUnknown" + key);
	}
}
