/*
 * Copyright (c) 2014. Shaking Earth Digital
 */

package com.shakingearthdigital.contentdownloadplugin.models.within;

import android.util.Log;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@SuppressWarnings("UnusedDeclaration")
public class ContentLocal extends Content {

    private static final long serialVersionUID = 1869437387890159079L;

    @Deprecated
    private String thumbnail;
    @Deprecated
    private String detailThumbnail;

    private String path;
    private ArrayList<Camera> cameras;

    @JsonIgnore
    private String jsonPath;

    @Deprecated
    @JsonIgnore
    public String getThumbnail() {
        return thumbnail;
    }
    @Deprecated
    @JsonProperty("thumbnail")
    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Deprecated
    @JsonIgnore
    public String getDetailThumbnail() {
        return detailThumbnail;
    }

    @Deprecated
    @JsonProperty("detail_thumbnail_link")
    public void setDetailThumbnail(String detailThumbnail) {
        this.detailThumbnail = detailThumbnail;
        if (this.detailThumbnail == null) {
            this.detailThumbnail = thumbnail;
        }
    }

    @JsonProperty("path")
    public String getPath() {
        return path;
    }
    public void setPath(String path) {
        if(this.path != null && this.path.toLowerCase().contains("vrsecardboard")) return; //don't change the filepath when loading content from cardboard
        this.path = path;
    }

    @JsonProperty("cameras")
    public ArrayList<Camera> getCameras() {
        return cameras;
    }
    public void setCameras(ArrayList<Camera> cameras) {
        this.cameras = cameras;
    }


    public String getJsonPath() {
        return jsonPath;
    }
    public void setJsonPath(String jsonPath) {
        this.jsonPath = jsonPath;
    }

    @JsonAnySetter
    public void handleUnknown(String key, Object value) {
        //unknown properties
        Log.w("Content handleUnknown", key);
    }
}
