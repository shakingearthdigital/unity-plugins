/*
 * Copyright (c) 2015. Shaking Earth Digital
 */

package com.shakingearthdigital.contentdownloadplugin.models.within;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.shakingearthdigital.contentdownloadplugin.utils.SELogUtil;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("UnusedDeclaration")
public class Content implements Serializable {

    private static final long serialVersionUID = 7279188513888521904L;
    protected int id;
    protected String title;
    protected String author;
    protected String description = "";
    protected String eventId;
    private String subtitlePath;
    private String titleImagePath;
    protected String contentWarning;
    private int audioSampleRate;
    private String playback = "concurrent";

    private ArrayList<ContentImage> images = new ArrayList<>();

    @JsonIgnore
    private String titleImage;
    @JsonIgnore
    private String descriptionImage;

    public Content(){

    }

    @JsonProperty("audio_sample_rate")
    public int getAudioSampleRate(){return audioSampleRate;}
    public void setAudioSampleRate(int audioSampleRate){
        this.audioSampleRate = audioSampleRate;
    }

    @JsonProperty("id")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("author")
    public String getAuthor() {
        return author;
    }
    public void setAuthor(String author) {
        this.author = author;
    }


    @JsonProperty("description")
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("subtitles")
    public String getSubtitlePath() {
        return subtitlePath;
    }

    @JsonProperty("titleImages")
    public String getTitleImagePath(){
        return titleImagePath;
    }

    public void setSubtitlePath(String subtitlePath) {
        this.subtitlePath = subtitlePath;
    }

    public void setTitleImagePath(String titleImagePath){this.titleImagePath = titleImagePath;}

    public String getTitleImage() {
        return titleImage;
    }

    public void setTitleImage(String titleImg) {
        this.titleImage = titleImg;
    }

    public String getDescriptionImage() {
        return descriptionImage;
    }

    public void setDescriptionImage(String descriptionImage) {
        this.descriptionImage = descriptionImage;
    }

    @JsonProperty("warning")
    public String getContentWarning() {
        return contentWarning;
    }
    public void setContentWarning(String contentWarning) {
        this.contentWarning = contentWarning;
    }


    @JsonProperty("eventId")
    public String getEventId() {
        return eventId;
    }
    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    @JsonProperty("images")
    public ArrayList<ContentImage> getImages() {
        return images;
    }
    public void setImages(ArrayList<ContentImage> images) {
        this.images = images;
    }

    @JsonProperty("playback")
    public String getPlayback() {
        return playback;
    }
    public void setPlayback(String playback) {
        this.playback = playback;
    }


    @JsonAnySetter
    public void handleUnknown(String key, Object value) {
        //unknown properties
        SELogUtil.logW("Content handleUnknown" + key);
    }
    @Override
    public boolean equals(Object o) {

        if (o instanceof ContentLink){
            return id == ((ContentLink)o).getId();
        }

        return super.equals(o);
    }
}
