package com.shakingearthdigital.contentdownloadplugin.models.within;

import android.util.Log;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;

public class Subtitles {
	
	private ArrayList<String> languages;
	private Subtitle[] subtitles;
	
	public static class Subtitle {
		private long startTime;
		private long endTime;
		private int id;
		private int track;
		private float depth;
		private float elevation;
		
		private HashMap<String, String> text;

		public Subtitle(){}

		@JsonProperty("startTime")
		public long getStartTime() {
			return startTime;
		}

		@JsonProperty("id")
		public int getId() {
			return id;
		}

		@JsonProperty("endTime")
		public long getEndTime() {
			return endTime;
		}

		public void setEndTime(long endTime) {
			this.endTime = endTime;
		}

		public void setId(int id) {
			this.id = id;
		}

		@JsonProperty("track")
		public int getTrack() {
			return track;
		}

		public void setTrack(int track) {
			this.track = track;
		}

		public void setStartTime(long startTime) {
			this.startTime = startTime;
		}

		@JsonProperty("text")
		public HashMap<String, String> getText() {
			return text;
		}

		public void setText(HashMap<String, String> text) {
			this.text = text;
		}

		@JsonProperty("depth")
		public float getDepth() {
			return depth;
		}

		public void setDepth(float depth) {
			this.depth = depth;
		}

		@JsonProperty("elevation")
		public float getElevation() {
			return elevation;
		}

		public void setElevation(float elevation) {
			this.elevation = elevation;
		}

	}

    @JsonProperty("languages")
	public ArrayList<String> getLanguages() {
		return languages;
	}

	public void setLanguages(ArrayList<String> languages) {
		this.languages = languages;
	}
	

    @JsonAnySetter
    public void handleUnknown(String key, Object value) {
    	//unknown properties
        Log.w("Subtitles handleUnknown", key);
    }

	public Subtitle[] getSubtitles() {
		return subtitles;
	}

	public void setSubtitles(Subtitle[] subtitles) {
		this.subtitles = subtitles;
	}

}
