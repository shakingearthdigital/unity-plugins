package com.shakingearthdigital.contentdownloadplugin.models.within;

import android.util.Log;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Brendon on 12/17/2015.
 *
 * Camera for streaming content
 */
public class StreamingCamera implements Serializable{
    private static final long serialVersionUID = 298831202734606474L;
    public static final String STREAMING_PATH = "Stream";
    private String videoMono;
    private String audioMono; //TODO?
    private String video;
    private String videoFormat;
    private String[] audio;

    @JsonProperty("video_stereo")
    public String getVideo() {return video;}
    public void setVideo(String video) {this.video = video;}

    @JsonProperty("video_mono")
    public String getVideoMono() {return videoMono;}
    public void setVideoMono(String videoMono) {this.videoMono = videoMono;}

    @JsonProperty("video_format")
    public String getVideoFormat() {return videoFormat;}
    public void setVideoFormat(String videoFormat) {this.videoFormat = videoFormat;}

    @JsonProperty("audio")
    public String[] getAudio() {return audio;}
    public void setAudio(String[] audio) {this.audio = audio;}


    @SuppressWarnings("UnusedDeclaration")
    @JsonAnySetter
    public void handleUnknown(String key, Object value) {
        //unknown properties
        Log.w("StreamingCamera", "handleUnknown : "+key);
    }
}
