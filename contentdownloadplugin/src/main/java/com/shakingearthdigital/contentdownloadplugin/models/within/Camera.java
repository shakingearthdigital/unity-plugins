/*
 * Copyright (c) 2014. Shaking Earth Digital
 */

package com.shakingearthdigital.contentdownloadplugin.models.within;


import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.shakingearthdigital.contentdownloadplugin.utils.SELogUtil;


import java.io.Serializable;

@SuppressWarnings("UnusedDeclaration")
public class Camera implements Serializable {
    private static final long serialVersionUID = 298831202734606474L;

    private String video;
    private String videoHD;
    private String videoFormat;
    private String videoFormatHD;

    @Deprecated
    private String audio1;
    @Deprecated
    private String audio2;
    @Deprecated
    private String audio3;
    @Deprecated
    private String audio4;
    private String[] audio;

    private StreamingCamera streaming;

    @JsonProperty("video")
    public String getVideo() {
        return video;
    }
    public void setVideo(String video) {
        this.video = video;
    }

    @JsonIgnore
    public String getVideoHD() {
        return videoHD;
    }
    @JsonProperty("videoHD")
    public void setVideoHD(String videoHD) {
        this.videoHD = videoHD;
    }

    @JsonProperty("video_format")
    public String getVideoFormat() {
        return videoFormat;
    }
    public void setVideoFormat(String videoFormat) {
        this.videoFormat = videoFormat;
    }

    @JsonIgnore
    public String getVideoFormatHD() {
        if (videoFormatHD == null){
            return videoFormat;
        }
        return videoFormatHD;
    }
    @JsonProperty("video_formatHD")
    public void setVideoFormatHD(String videoFormatHD) {
        this.videoFormatHD = videoFormatHD;
    }

    @JsonIgnore
    public String getAudio1() {
        return audio1;
    }
    @JsonProperty("audio1")
    public void setAudio1(String audio1) {
        this.audio1 = audio1;
    }

    @JsonIgnore
    public String getAudio2() {
        return audio2;
    }
    @JsonProperty("audio2")
    public void setAudio2(String audio2) {
        this.audio2 = audio2;
    }

    @JsonIgnore
    public String getAudio3() {
        return audio3;
    }
    @JsonProperty("audio3")
    public void setAudio3(String audio3) {
        this.audio3 = audio3;
    }

    @JsonProperty("audio")
    public String[] getAudio() {
        return audio;
    }
    public void setAudio(String[] audio) {
        this.audio = audio;
    }

    @JsonIgnore
    public String getAudio4() {
        return audio4;
    }
    @JsonProperty("audio4")
    public void setAudio4(String audio4) {
        this.audio4 = audio4;
    }

    @JsonIgnore
    public StreamingCamera getStreaming() {
        return streaming;
    }
    @JsonProperty("streaming")
    public void setStreaming(StreamingCamera streaming) {
        this.streaming = streaming;
    }


    @SuppressWarnings("UnusedDeclaration")
    @JsonAnySetter
    public void handleUnknown(String key, Object value) {
        //unknown properties
        SELogUtil.logW("Camera handleUnknown" + key);
    }
}
