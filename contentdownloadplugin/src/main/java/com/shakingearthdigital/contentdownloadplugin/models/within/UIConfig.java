package com.shakingearthdigital.contentdownloadplugin.models.within;

import android.util.Log;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UIConfig {
	private boolean displayOffline = true;
	private String bgImgUrl, titleImgUrl;
	
	public UIConfig(){}

	@JsonProperty("displayOffline")
	public boolean isDisplayOffline() {
		return displayOffline;
	}

	public void setDisplayOffline(boolean displayOffline) {
		this.displayOffline = displayOffline;
	}

	@JsonProperty("bgImgUrl")
	public String getBgImgUrl() {
		return bgImgUrl;
	}

	public void setBgImgUrl(String bgImgUrl) {
		this.bgImgUrl = bgImgUrl;
	}

	@JsonProperty("titleImgUrl")
	public String getTitleImgUrl() {
		return titleImgUrl;
	}

	public void setTitleImgUrl(String titleImgUrl) {
		this.titleImgUrl = titleImgUrl;
	}
	
    
    @SuppressWarnings("UnusedDeclaration")
    @JsonAnySetter
    public void handleUnknown(String key, Object value) {
        //unknown properties
        Log.w("ContentConfig", "handleUnknown : " + key);
    }
}
