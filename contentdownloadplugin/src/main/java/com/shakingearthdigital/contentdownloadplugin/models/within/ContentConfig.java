package com.shakingearthdigital.contentdownloadplugin.models.within;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.shakingearthdigital.contentdownloadplugin.utils.SELogUtil;

import java.util.Calendar;

public class ContentConfig {
	private int contentId;
	private String startTime;
	private Calendar startTimeCal;
	
	public ContentConfig(){}

    @JsonProperty("id")
	public int getContentId() {
		return contentId;
	}

	public void setContentId(int contentId) {
		this.contentId = contentId;
	}
	
	@JsonProperty("startTime")
	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	
    
    @SuppressWarnings("UnusedDeclaration")
    @JsonAnySetter
    public void handleUnknown(String key, Object value) {
        //unknown properties
        SELogUtil.logW("ContentConfig handleUnknown" + key);
    }

	public void setStartTimeCal(Calendar startTimeCal) {
		this.startTimeCal = startTimeCal;
	}
	
	public Calendar getStartTimeCal(){
		return startTimeCal;
	}
}
