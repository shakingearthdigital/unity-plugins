/*
 * Copyright (c) 2014. Shaking Earth Digital
 */

package com.shakingearthdigital.contentdownloadplugin.models.within;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.shakingearthdigital.contentdownloadplugin.utils.SELogUtil;

import java.util.ArrayList;

public class OnlineContent {
	
	private ArrayList<ContentLink> content;
	private int contentVersion;
    private ArrayList<RemoveContent> removeContent;

    @JsonProperty("content_version")
    public int getContentVersion() {
        return contentVersion;
    }

    public void setContentVersion(int contentVersion) {
        this.contentVersion = contentVersion;
    }

    @JsonProperty("content")
	public ArrayList<ContentLink> getContentLinks() {
		return content;
	}
	public void setContentLinks(ArrayList<ContentLink> content) {
		this.content = content;
	}

    @JsonProperty("remove_content")
    public ArrayList<RemoveContent> getRemoveContent(){return removeContent;}

    @JsonAnySetter
    public void handleUnknown(String key, Object value) {
    	//unknown properties
        SELogUtil.logW("Content handleUnknown" + key);
    }
}
