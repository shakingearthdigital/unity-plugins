package com.shakingearthdigital.contentdownloadplugin.models.within;

/**
 * Created by Brendon on 12/22/2015.
 *
 * This is used to notify whoever that thumbnails were updated
 */
public class ThumbnailDownloadEvent {
    public static final int SUCCESS = 1;
    public int resultCode;
    public ThumbnailDownloadEvent(int resultCode){
        this.resultCode = resultCode;
    }
}
