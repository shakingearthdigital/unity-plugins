/*
 * Copyright (c) 2014. Shaking Earth Digital
 */

package com.shakingearthdigital.contentdownloadplugin.models.within;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.shakingearthdigital.contentdownloadplugin.utils.SELogUtil;

import java.io.Serializable;
import java.util.ArrayList;

//@SuppressWarnings("UnusedDeclaration")
public class ContentLink extends Content implements Serializable{

    private static final long serialVersionUID = -7603843742914571917L;

    private String fileSize;
    private boolean featuredContent = false;
    private ArrayList<Camera> cameras = new ArrayList<>();
    private String releaseDate;
    private String featureUntilDate;
    private String geoRestrType;
    private ArrayList<String> geoRestrList;
    private boolean comingSoon;

    @JsonProperty("coming_soon")
    public boolean getComingSoon(){return comingSoon;}
    public void setComingSoon(boolean comingSoon){
        this.comingSoon = comingSoon;
    }

    @JsonProperty("featured")
    public boolean isFeaturedContent() {
        return featuredContent;
    }
    public void setFeaturedContent(boolean featuredContent) {
        this.featuredContent = featuredContent;
    }

    @JsonProperty("release_date")
    public String getReleaseDate() { return releaseDate; }
    public void setReleaseDate(String releaseDate) { this.releaseDate = releaseDate; }

    @JsonProperty("feature_until_date")
    public String getFeatureUntilDate() { return featureUntilDate; }
    public void setFeatureUntilDate(String featureUntilDate) { this.featureUntilDate = featureUntilDate; }

    @JsonProperty("content_filesize")
    public String getFileSize() {
        return fileSize;
    }
    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    @JsonProperty("cameras")
    public ArrayList<Camera> getCameras() {
        return cameras;
    }
    public void setCameras(ArrayList<Camera> cameras) {
        this.cameras = cameras;
    }

    @JsonProperty("geo_restr_type")
    public String getGeoRestrType() { return geoRestrType; }
    public void setGeoRestrType(String geoRestrType) { this.geoRestrType = geoRestrType; }

    @JsonProperty("geo_restr_list")
    public ArrayList<String> getGeoRestrList() { return geoRestrList; }
    public void setGeoRestrList(ArrayList<String> geoRestrList) { this.geoRestrList = geoRestrList; }

    @SuppressWarnings("UnusedDeclaration")
    @JsonAnySetter
    public void handleUnknown(String key, Object value) {
        //unknown properties
        SELogUtil.logW("ContentLink handleUnknown" + key);
    }
}
