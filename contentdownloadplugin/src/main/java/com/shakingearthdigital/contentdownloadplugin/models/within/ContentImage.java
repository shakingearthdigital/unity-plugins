/*
 * Copyright (c) 2014. Shaking Earth Digital
 */

package com.shakingearthdigital.contentdownloadplugin.models.within;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.shakingearthdigital.contentdownloadplugin.utils.SELogUtil;

import java.io.Serializable;

@SuppressWarnings("UnusedDeclaration")
public class ContentImage implements Serializable{

    private static final long serialVersionUID = -6946244526864235414L;
    private String type;
    private String link;

    public ContentImage(){}

    public ContentImage(String type, String link) {
        this.type = type;
        this.link = link;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("link")
    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @JsonAnySetter
    public void handleUnknown(String key, Object value) {
        //unknown properties
        SELogUtil.logW("Content handleUnknown" + key);
    }
}
