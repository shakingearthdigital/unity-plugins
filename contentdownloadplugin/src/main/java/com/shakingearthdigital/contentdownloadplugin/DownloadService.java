package com.shakingearthdigital.contentdownloadplugin;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaScannerConnection;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.SystemClock;
import android.util.Log;

import com.shakingearthdigital.contentdownloadplugin.managers.ContentManager;
import com.shakingearthdigital.contentdownloadplugin.models.DownloadItem;
import com.shakingearthdigital.contentdownloadplugin.utils.ContentUtil;
import com.shakingearthdigital.contentdownloadplugin.utils.SELogUtil;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by jacob on 9/7/2016.
 */
public class DownloadService extends IntentService {
    private SELogUtil log = new SELogUtil("DownloadService");
    private Context context;

    private static final String EXTRA_CONTENT_ID = "CONTENT.ID";
    private static final String EXTRA_DOWNLOAD_ITEM = "CONTENT.ITEM";

    private static final String ACTION_DOWNLOAD_CONTENT = "com.shakingearthdigital.vrsecardboard.services.action.Content";
    private static final String ACTION_CANCEL_DOWNLOAD = "com.shakingearthdigital.vrsecardboard.services.action.Cancel";

    private static final String NOTIFICATION_CANCEL = "dismiss";

    private boolean useExternalStorage;
    private NotificationManager mNotificationManager;

    public static NetworkChangeReceiver networkChangeReceiver = new NetworkChangeReceiver();
    volatile static ArrayList<DownloadItem> mDownloadQueue = new ArrayList<>();
    public static STATE serviceState = STATE.INACTIVE;
    public static Listener callback;


    public static STATE getServiceState() {
        return serviceState;
    }

    public enum STATE {
        DOWNLOADING(0),
        WAITING(1),
        INACTIVE(2);

        private int iValue;
        STATE(int i) { iValue = i; }
        public int GetValue() {  return iValue; }
    }


    public static void setListener(Listener listener) {
        callback = listener;
    }

    public interface Listener {
        void onServiceStateChanged(int state);
        void onDownloadAdded(int contentId, int contentState);
        void onProgressChanged(int contentId, float progress);
        void onDownloadCompleted(int contentId, boolean onlyAudio);
        void onDownloadCanceled(int contentId);
        void onDownloadFailed(int contentId, String error);
        void onContentDeleted(int contentId);
    }


    public DownloadService() {
        super("DownloadService");
    }

    private final IBinder mBinder = new DownloadServiceBinder();
    public class DownloadServiceBinder extends Binder {
        public DownloadService getService() {
            return DownloadService.this;
        }
    }

    public static void downloadContentService(Context context, int contentId, ArrayList<String> urls) {
        Log.d("downloadContentService", "");
        Intent intent = new Intent(context, DownloadService.class);
        intent.setAction(ACTION_DOWNLOAD_CONTENT);
        intent.putExtra(EXTRA_DOWNLOAD_ITEM, new DownloadItem(contentId, urls));
        context.startService(intent);
    }

    public static void downloadContentService(Context context, DownloadItem item) {
        Log.d("downloadContentService", "");
        Intent intent = new Intent(context, DownloadService.class);
        intent.setAction(ACTION_DOWNLOAD_CONTENT);
        intent.putExtra(EXTRA_DOWNLOAD_ITEM, item);
        context.startService(intent);
    }

    public static void cancelDownloadService(Context context, int contentId) {
        Log.d("cancelDownloadService", "");
        Intent intent = new Intent(context, DownloadService.class);
        intent.setAction(ACTION_CANCEL_DOWNLOAD);
        intent.putExtra(EXTRA_CONTENT_ID, contentId);
        intent.putExtra(NOTIFICATION_CANCEL, contentId);
        context.startService(intent);
    }

    public static void resumeDownloadService(Context context) {
        Log.d("resumeDownloadService", "");
        if (!mDownloadQueue.isEmpty()) {
            for (DownloadItem item : mDownloadQueue) {
                item.notify_Builder = null;
                downloadContentService(context, item);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getBaseContext();
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        useExternalStorage = ContentManager.getDownloadPref(context);
        if (serviceState != STATE.WAITING && mNotificationManager != null) mNotificationManager.cancelAll();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(networkChangeReceiver, intentFilter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        log.d("onDestroy called");
        unregisterReceiver(networkChangeReceiver);
    }

    /**
     * Performed on an item as soon as it is added to the service
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        log.d("onStartCommand");

        if (intent != null) {
            String action = intent.getAction();

            switch(action) {
                case ACTION_DOWNLOAD_CONTENT:
                    addToDownloadQueue(intent);
                    break;
                case ACTION_CANCEL_DOWNLOAD:
                    cancelDownload(intent);
                    break;
            }
        }

        super.onStartCommand(intent, flags, startId);
        return START_REDELIVER_INTENT;
    }

    /**
     * Performed on an item when it is up to download
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        log.d("onHandleIntent");

        if (serviceState == STATE.WAITING) return;

        if (intent != null) {
            String action = intent.getAction();

            // Handle action
            switch(action) {
                case ACTION_DOWNLOAD_CONTENT:
                    startDownload(intent);
                    break;
                case ACTION_CANCEL_DOWNLOAD:
                    // Action already handled in onStartCommand
                    break;
                default:
                    break;
            }

        }
    }


    public void startDownload(Intent intent) {
        log.d("startDownload");

        if (serviceState == STATE.WAITING) {
            return;
        }
        else if (serviceState != STATE.DOWNLOADING) {
            serviceState = STATE.DOWNLOADING;
            if (callback != null) callback.onServiceStateChanged(STATE.DOWNLOADING.GetValue());
        }

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "WithinDownload");
        wakeLock.acquire();

        // ensure it hasn't been canceled while waiting in the queue, mark it as downloading
        DownloadItem item = (DownloadItem) intent.getSerializableExtra(EXTRA_DOWNLOAD_ITEM);

        if (item.state == DownloadItem.STATE.QUEUED) {
            item.state = DownloadItem.STATE.DOWNLOADING;
        }
        else return;

        File dir = getDownloadDirectory(item.contentId);
        long downloadSize = computeDownloadSize(item, dir);
        long usableStorage = dir.getUsableSpace() + 74*1024*1024;

        if (item.useNotification()) {
            Log.d("WAITING_DEBUG", "start download : useNotification : notify");
            item.buildNotification(context);
            mNotificationManager.notify(item.contentId, item.getNotification());
        }


        log.d(String.format("startDownload : downloadSize=%d, usableStorage=%d", downloadSize, usableStorage));

        boolean downloadSuccess = false;

        if (downloadSize < usableStorage) {
            log.d("startDownload : hasFreeSpace");
            item.setSize(downloadSize);
            downloadSuccess = downloadUrls(item, dir); // download the files

            if (downloadSuccess) {

                if (item.writeJson) item.writeJson(context);

                log.d("startDownload : download success!");
                if (callback != null) callback.onDownloadCompleted(item.contentId, false);
            }
            else {
                // download failed, canceled, or is waiting
                if (serviceState == STATE.WAITING) {
                    mNotificationManager.notify(item.contentId, item.getNotification()); // update notification to show queued status
                    wakeLock.release();
                    return;
                }
            }
        }
        else {
            log.d("startDownload : not enough free space");
            item.state = DownloadItem.STATE.ERROR;
            if (item.useNotification()) mNotificationManager.notify(item.contentId, item.getNotification("Failed: insufficient space"));
            if (callback != null) callback.onDownloadFailed(item.contentId, "Failed: insufficient space");
        }

        // Done with item
        mNotificationManager.cancel(item.contentId);
        mDownloadQueue.remove(item);
        wakeLock.release();

        // If there are no more downloads, let service become inactive
        if (mDownloadQueue.isEmpty()) {
            serviceState = STATE.INACTIVE;
            if (callback != null) callback.onServiceStateChanged(STATE.INACTIVE.GetValue());
        }
    }

    public boolean downloadUrls(DownloadItem item, File dir){
        log.d("downloadUrls");
        boolean success = false;
        for (int tries = 0; tries < 5; tries++) {
            if (tries > 0) {
                int wait = (int) (1000 * Math.pow(2, tries));
                SystemClock.sleep(wait);
            }

            success = true;
            for (int i = item.urls.size() - 1; i > -1; i--) {
                String url = item.urls.get(i);

                if (serviceState == STATE.WAITING) return false;

                if (item.state != DownloadItem.STATE.CANCELED && url != null) {
                    log.d(String.format("downloadUrls : downloading %s", item.urls.get(i)));
                    boolean downloadSuccess = downloadFile(item, url, ContentUtil.getContentFileFromUrl(dir, url), tries > 0);
                    if (downloadSuccess) item.urls.remove(i);
                    success = success && downloadSuccess;
                }
                else if (item.state == DownloadItem.STATE.CANCELED) {
                    log.d("downloadUrls : download cancelled");
                    return false;
                }
            }
            if (success) {
                log.d("downloadUrls : downloads complete!");
                return true;
            }
        }

        return false;
    }


    public boolean downloadFile(DownloadItem item, String url, File destination, boolean retry) {
        long downloaded = 0;
        Request.Builder builder = new Request.Builder().url(url).get();

        if (destination.exists()) {
            downloaded = destination.length();
            if (!retry) {
                updateDownloadProgress(item, downloaded);
            }
            builder.addHeader("Range", "bytes=" + downloaded + "-");
        }

        OkHttpClient httpClient = new OkHttpClient();
        Call call = httpClient.newCall(builder.build());

        try {
            Response response = call.execute();
            if (response.code() == 200 || response.code() == 206) {
                log.d("downloadContent response=" + response.code());
                InputStream inputStream = null;
                BufferedOutputStream fos = new BufferedOutputStream(
                        downloaded > 0 ? new FileOutputStream(destination, true) : new FileOutputStream(destination));
                try {
                    inputStream = response.body().byteStream();
                    byte[] buff = new byte[1024 * 4];

                    while (true) {
                        if (item.state == DownloadItem.STATE.CANCELED) return false; // break

                        if (serviceState == STATE.WAITING) return false;

                        int read = inputStream.read(buff);
                        if (read == -1) {
                            log.d("read == -1");
                            break;
                        }

                        //write buff
                        fos.write(buff, 0, read);

                        updateDownloadProgress(item, read);

                        // log.d(String.format("downloadFile : %f", item.getProgress()));
                    }
                    if (item.state != DownloadItem.STATE.CANCELED) {
                        MediaScannerConnection.scanFile(this, new String[]{destination.getAbsolutePath()}, null, null);
                    }
                    log.d("downloadContent " + item.contentId + " " + destination + " download complete");
                    return true;
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    fos.flush();
                    fos.close();
                    if (inputStream != null) {
                        inputStream.close();
                    }
                }
            }  else {
                if (response.code() == 416) return true;    // problem with Within server config?
                log.e("downloadContent : unexpected response " + response.code());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        log.e("downloadContent " + item.contentId + " " + destination + " download failed");
        return false;
    }


    public void addToDownloadQueue(Intent intent) {
        DownloadItem item = (DownloadItem) intent.getSerializableExtra(EXTRA_DOWNLOAD_ITEM);
        item.state = DownloadItem.STATE.QUEUED;

        if (!inDownloadQueue(item.contentId)) {
            log.d("addToDownloadQueue : added " + item.contentId);
            mDownloadQueue.add(item);
            if (callback != null) callback.onDownloadAdded(item.contentId, item.state.GetValue());
        }
        else {
            log.d("addToDownloadQueue : already in " + item.contentId);
            for (int i=0; i<mDownloadQueue.size(); i++) {
                if (mDownloadQueue.get(i).contentId == item.contentId) {
                    log.d("addToDownloadQueue : updated in queue " + item.contentId);
                    mDownloadQueue.set(i, item);
                }
            }
        }
    }

    public boolean cancelDownload(Intent intent) {
        DownloadItem item = getDownloadItem(intent.getIntExtra(EXTRA_CONTENT_ID, -1));
        log.d("cancelDownload : " + item.contentId);

        if (intent.hasExtra(NOTIFICATION_CANCEL)) {
            mNotificationManager.cancel(item.contentId);
        }

        if (inDownloadQueue(item.contentId)) {
            item.state = DownloadItem.STATE.CANCELED;
            mDownloadQueue.remove(item);
            if (callback != null)callback.onDownloadCanceled(item.contentId);
        }

        if (mDownloadQueue.size() == 0) {
            serviceState = STATE.INACTIVE;
            if (callback != null)callback.onServiceStateChanged(STATE.INACTIVE.GetValue());
        }

        return true;
    }

    public boolean isCanceled(int contentId) {
        DownloadItem.STATE state = getDownloadItem(contentId).state;
        if (state == DownloadItem.STATE.CANCELED) return true;
        else return false;
    }

    /**
     *  When state is actually downloading
     */
    public boolean isDownloading(int contentId) {
        DownloadItem.STATE state = getDownloadItem(contentId).state;
        if (state == DownloadItem.STATE.DOWNLOADING) return true;
        else return false;
    }

    public boolean isQueued(int contentId) {
        DownloadItem.STATE state = getDownloadItem(contentId).state;
        if (state == DownloadItem.STATE.QUEUED) return true;
        else return false;
    }

    /**
     * content is either downloading or is queued to be downloaded.
     * returns false if it's canceled
     */
    public boolean inDownloadQueue(int contentId) {
        DownloadItem item = getDownloadItem(contentId);

        if (item != null) {
            if (item.state == DownloadItem.STATE.DOWNLOADING || item.state == DownloadItem.STATE.QUEUED) return true;
        }

        return false;
    }


    public int getActiveDownload() {
        if (!mDownloadQueue.isEmpty()) {
            return mDownloadQueue.get(0).contentId;
        }
        return -1;
    }


    public int getDownloadCount() {
        return mDownloadQueue.size();
    }


    public int getPositionInQueue(int contentId) {
        for (int i = 0; i < mDownloadQueue.size(); i++) {
            if (mDownloadQueue.get(i).contentId == contentId) {
                return i;
            }
        }
        return -1;
    }

    public DownloadItem getDownloadItem(int contentId) {
        if (mDownloadQueue != null && !mDownloadQueue.isEmpty()) {
            for (DownloadItem item : mDownloadQueue) {
                if (item.contentId == contentId) {
                    return item;
                }
            }
        }
        return null;
    }


    public void updateDownloadProgress(DownloadItem item, long downloaded) {
        boolean percentIncreased = item.addProgress(downloaded);

        // if the value increased, update
        if (percentIncreased) {
            Log.d("updateDownloadProgress", "percent increased : useNotification()"+String.valueOf(item.useNotification()));
            if (item.useNotification()) mNotificationManager.notify(item.contentId, item.getNotification());
            if (callback != null) callback.onProgressChanged(item.contentId, item.getProgress());
        }
    }

    public float getProgress(int contentId) {
        if (inDownloadQueue(contentId)) {
            return getDownloadItem(contentId).getProgress();
        }
        return 0;
    }

    public File getDownloadDirectory(int contentId) {
        log.d("getDownloadDirectory");

        File dir;
        if (useExternalStorage) dir = ContentManager.getContentDirExternalSD(context, contentId);
        else dir = ContentManager.getContentDir(context, contentId);

        if (dir == null) {
            return null;
        }
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                return null;
            }
        }

        return dir;
    }


    /**
     * Currently removes urls that are already downloaded
     * As well as return the size of urls still left
     */
    public long computeDownloadSize(DownloadItem item, File dir) {
        log.d("computeDownloadSize..");
        long size = 0;
        ArrayList<String> urlsToDownload = new ArrayList<String>();
        for (String url : item.urls) {
            if (item.state != DownloadItem.STATE.CANCELED) {
                long contentSize = ContentUtil.getContentSize(url);
                size += contentSize;
                File downloadedFile = ContentUtil.getContentFileFromUrl(dir, url);
                if (downloadedFile.exists() && downloadedFile.length() == contentSize) {
                    log.d("computeDownloadSize : file complete=" + downloadedFile.getName());
                    item.addProgress(contentSize);
                } else urlsToDownload.add(url);
            }
        }

        item.setUrls(urlsToDownload);
        return size;
    }


    public static void notifyNetworkChange(Context context, boolean canDownload) {
        if (!canDownload && serviceState == STATE.DOWNLOADING) {
            serviceState = STATE.WAITING;
            if (callback != null) callback.onServiceStateChanged(STATE.WAITING.GetValue());
        }
        else if (canDownload && serviceState == STATE.WAITING) {
            serviceState = STATE.DOWNLOADING;
            if (callback != null) callback.onServiceStateChanged(STATE.DOWNLOADING.GetValue());
            resumeDownloadService(context);
        }
    }
}
