package com.shakingearthdigital.contentdownloadplugin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;

import com.shakingearthdigital.contentdownloadplugin.managers.ContentManager;
import com.shakingearthdigital.contentdownloadplugin.utils.SELogUtil;

/**
 * Created by jacob on 9/9/2016.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {
    private static final SELogUtil log = new SELogUtil(NetworkChangeReceiver.class.getSimpleName());
    boolean wifiConnected;
    boolean dataConnected;
    boolean canUseData;
    DownloadService.STATE state;

    @Override
    public void onReceive(Context context, Intent intent) {
        wifiConnected = isWifiConnected(context);
        canUseData = !ContentManager.getWifiDownloadOnlyPref(context);
        dataConnected = hasInternet(context);
        state = DownloadService.serviceState;

        log.d("NetworkChangeReceiver : canDownload="+canDownload());

        DownloadService.notifyNetworkChange(context, canDownload());
    }


    public boolean canDownload() {
        if (wifiConnected || (canUseData && dataConnected)) return true;
        else return false;
    }


    public static boolean checkConnectivity(Context context) {
        ConnectivityManager connectionManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectionManager.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    public static boolean isWifiConnected(Context context) {
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            for (Network network : connManager.getAllNetworks()) {
                NetworkInfo networkInfo = connManager.getNetworkInfo(network);
                if (networkInfo != null && networkInfo.getType() == ConnectivityManager.TYPE_WIFI && networkInfo.isConnected()){
                    return true;
                }
            }
        } else {
            //deprecated in API 23
            @SuppressWarnings("deprecation") NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (mWifi != null && mWifi.isConnected()) {
                return true;
            }
        }

        return false;
    }

    public static boolean isInternetAccessible(){
        return true;
    }

    public static boolean hasInternet(Context context){
        boolean isNetworkAvailable = checkConnectivity(context);
        boolean isInternetAvailable = isNetworkAvailable && isInternetAccessible();
        return isInternetAvailable;
    }


}
