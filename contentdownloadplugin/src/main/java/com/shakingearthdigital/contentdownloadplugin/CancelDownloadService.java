package com.shakingearthdigital.contentdownloadplugin;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.shakingearthdigital.contentdownloadplugin.utils.SELogUtil;

/**
 * Created by jacob on 9/8/2016.
 */
public class CancelDownloadService extends IntentService {


    private static final String EXTRA_CONTENT_ID = "contentID";

    public static Intent getLaunchIntent(Context context, int contentId) {
        Intent intent = new Intent(context, CancelDownloadService.class);
        intent.putExtra(EXTRA_CONTENT_ID, contentId);
        return intent;
    }

    public CancelDownloadService() {
        super("CancelDownloadService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            if (intent.hasExtra(EXTRA_CONTENT_ID)){
                SELogUtil.log("CancelDownloadActivity post id " + intent.getIntExtra(EXTRA_CONTENT_ID, -1));
                DownloadService.cancelDownloadService(this, intent.getIntExtra(EXTRA_CONTENT_ID, -1));
                //EventBus.getDefault().post(new NotificationCancelEvent(intent.getIntExtra(EXTRA_CONTENT_ID, -1)));
            }
        }
    }
}
