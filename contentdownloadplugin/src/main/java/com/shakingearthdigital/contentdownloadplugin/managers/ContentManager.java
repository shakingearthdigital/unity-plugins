package com.shakingearthdigital.contentdownloadplugin.managers;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.shakingearthdigital.contentdownloadplugin.models.within.ContentLink;
import com.shakingearthdigital.contentdownloadplugin.models.within.StreamingCamera;
import com.shakingearthdigital.contentdownloadplugin.utils.BaseUtil;
import com.shakingearthdigital.contentdownloadplugin.utils.ContentUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by jacob on 9/8/2016.
 */
public class ContentManager {
    private static String KEY_PREFS = "Prefs";
    private static String KEY_USE_WIFI_ONLY = "USE_WIFI_ONLY";
    private static String KEY_CONTENT_DIR = "CONTENT_DIR";
    private static String KEY_DIR_PREFIX = "DIR_PREFIX";
    public static final String CONTENT_SAVE_LOCATION = "CONTENT_SAVE_LOCATION";
    private static String AUDIO_DOWNLOAD_PREFIX = "audio_download_";

    public static File getContentDirExternalSD(Context context, int contentId) {
        String CONTENT_DIR = getContentDir(context);
        String CONTENT_PREFIX = getContentDirChildPrefix(context);

        File[] files = context.getExternalFilesDirs(CONTENT_DIR);
        for (int i = files.length - 1; i > - 1; i--) {
            if (files[i] != null) {
                return new File(files[i], String.format(Locale.getDefault(), "%s_%d", CONTENT_PREFIX, contentId));
            }
        }
        return null;
    }

    public static File getContentDir(Context context, int contentId) {
        String CONTENT_PREFIX = getContentDirChildPrefix(context);
        return new File(getContentRoot(context), String.format(Locale.getDefault(), "%s_%d", CONTENT_PREFIX, contentId));
    }


    public static File getContentFileExternalSD(Context context, int contentId) {
        String CONTENT_DIR = getContentDir(context);
        String CONTENT_PREFIX = getContentDirChildPrefix(context);

        File[] files = context.getExternalFilesDirs(CONTENT_DIR);
        for (int i = files.length - 1; i > - 1; i--) {
            if (files[i] != null) {
                return new File(files[i], String.format(Locale.getDefault(), "%s_%d.json", CONTENT_PREFIX, contentId));
            }
        }
        return null;
    }

    public static File getContentFile(Context context, int contentId) {
        return new File(getContentRoot(context), "content_"+contentId+".json");
    }

    /**
     * retrieves files folder inside package directory on device storage
     */
    public static File getContentRoot(Context context) {
        String CONTENT_DIR = getContentDir(context);
        File root = new File(context.getExternalFilesDir(null), CONTENT_DIR);
        if (!root.exists()) root.mkdir();
        return root;
    }

    public static void setDownloadPref(Context context, boolean isExternalPreferred){
        Log.d("setDownloadPref", "useExternal="+isExternalPreferred);
        context.getSharedPreferences(KEY_PREFS, 0).edit().putBoolean(CONTENT_SAVE_LOCATION, isExternalPreferred).commit();
    }

    public static boolean getDownloadPref(Context context){
        return context.getSharedPreferences(KEY_PREFS, 0).getBoolean(CONTENT_SAVE_LOCATION, false);
    }

    public static void setWifiDownloadOnlyPref(Context context, boolean useWifiOnly) {
        Log.d("setWifiPref", "useWifiOnly="+useWifiOnly);
        context.getSharedPreferences(KEY_PREFS, 0).edit().putBoolean(KEY_USE_WIFI_ONLY, useWifiOnly).commit();
    }

    public static boolean getWifiDownloadOnlyPref(Context context) {
        return context.getSharedPreferences(KEY_PREFS, 0).getBoolean(KEY_USE_WIFI_ONLY, false);
    }

    public static void setContentDir(Context context, String dir) {
        context.getSharedPreferences(KEY_PREFS, 0).edit().putString(KEY_CONTENT_DIR, dir).commit();
    }

    public static String getContentDir(Context context) {
        return context.getSharedPreferences(KEY_PREFS, 0).getString(KEY_CONTENT_DIR, "WITHIN");
    }

    public static void setContentDirChildPrefix(Context context, String dir) {
        context.getSharedPreferences(KEY_PREFS, 0).edit().putString(KEY_DIR_PREFIX, dir).commit();
    }

    public static String getContentDirChildPrefix(Context context) {
        return context.getSharedPreferences(KEY_PREFS, 0).getString(KEY_DIR_PREFIX, "Content");
    }

    public static boolean isDownloaded(Context context, int contentId) {
        if (ContentManager.getContentFile(context, contentId).exists()) return true;
        if (ContentManager.getContentFileExternalSD(context, contentId) != null &&
                ContentManager.getContentFileExternalSD(context, contentId).exists()) return true;
        else return false;
    }

    public static boolean deleteContent(Context context, int contentId) {
        if (isDownloaded(context, contentId)) {
            return removeFromDevice(context, contentId);
        }
        return false;
    }


    public static boolean removeFromDevice(Context context, int contentId) {
        Log.d("ContentManager", "removeFromDevice");
        boolean onExternal = false;
        File json = getContentFile(context, contentId);

        //delete .json file
        if(json.exists())  json.delete();
        else {
            // check external drive for the file
            json = getContentFileExternalSD(context, contentId);

            try {
                if (json.exists()) {
                    onExternal = true;
                    json.delete();
                } else return false; // doesn't exist anywhere..
            } catch (NullPointerException e) {
                return false; // doesn't exist anywhere..
            }
        }

        File contentDir;
        if (!onExternal) contentDir = getContentDir(context, contentId);
        else contentDir = getContentDirExternalSD(context, contentId);

        //delete contents and folder
        try {
            for (String file : contentDir.list()){
                Log.d("ContentManager", "file="+file);
                File fileToDelete = new File(contentDir, file);
                if (fileToDelete.exists()) {
                    Log.d("ContentManager", fileToDelete.getAbsolutePath());
                    fileToDelete.delete();
                } else {
                    Log.e("ContentManager", "can't find in path="+fileToDelete.getAbsolutePath());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }

        return contentDir.delete();
    }

    public static void removeStreamedAudio(Context context, int contentId){
        Log.d("ContentManager", "removeStreamAudio");
        File contentDir = new File(ContentManager.getContentDir(context, contentId).getAbsolutePath());
        if(!contentDir.exists()) contentDir = new File(ContentManager.getContentDirExternalSD(context, contentId).getAbsolutePath());

        //delete contents and folder
        try {
            for (String file : contentDir.list()) {
                new File(contentDir, file).delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        contentDir.delete();
    }


    public static String getLocalVideoPath(Context context, int contentId, String videoUrl) {
        File folder = new File(ContentManager.getContentDir(context, contentId).getAbsolutePath());
        File video = new File(folder, ContentUtil.getFileNameFromURL(videoUrl));
        return video.getAbsolutePath();
    }

    // FIXME: externalSD?
    public static String[] getLocalAudioPaths(Context context, int contentId, String[] audioPathLinks) {
        for (String string : audioPathLinks) Log.d("Debug", "audio="+string);

        if (isAudioDownloadComplete(context, contentId, audioPathLinks)) {

            String[] localAudioArr = new String[4];

            File folder = new File(ContentManager.getContentDir(context, contentId).getAbsolutePath());

            for (int i = 0; i < audioPathLinks.length; i++) {
                File audio = new File(folder, ContentUtil.getFileNameFromURL(audioPathLinks[i]));
                if (audio.exists()) localAudioArr[i] = audio.getAbsolutePath();
                else return null;
            }

            return localAudioArr;
        }

        return null;
    }


    public static boolean isAudioDownloadComplete(Context context, int contentId, String[] audioPathLinks) {
        Log.d("Debug", "isAudioDownloadCoplete : audioPathLinks:"+audioPathLinks);
        File folder = new File(ContentManager.getContentDir(context, contentId).getAbsolutePath());
                // + File.separator + StreamingCamera.STREAMING_PATH);

        for (String audioUrl : audioPathLinks) {
            // if it doesnt exist return false
            if (!new File(folder, ContentUtil.getFileNameFromURL(audioUrl)).exists()){
                Log.d("isAudioDownloadComplete", "files are missing");
                return false;
            }
        }

        Log.d("isAudioDownloadComplete", "all files present");
        return true;
    }

}
