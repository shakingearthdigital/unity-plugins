package com.shakingearthdigital.contentdownloadplugin.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaFormat;
import android.media.MediaMetadataRetriever;
import android.os.Build;

import java.io.IOException;

/**
 * Created by jacob on 9/8/2016.
 */
public class CapabilityChecker {

    private static String KEY_PREFS = "withinPrefs";
    private static String KEY_4K_CAPABLE = "4K_Capable";

    public static boolean is4KCapable(final Context context) {
        boolean isCapable;

        //manual override
        if (Build.DEVICE.equals("g3")){
            return true;
        } else if (Build.DEVICE.contains("htc_m8")){
            return false;
        } else {
            int capable = context.getSharedPreferences(KEY_PREFS, 0).getInt(KEY_4K_CAPABLE, -1);

            if (capable == -1){
                if(Build.VERSION.SDK_INT >= 21){
                    //check if supports with lollipop code
                    MediaCodecInfo mediaCodecInfo = selectCodec(getMimetype());

                    try {
                        isCapable = mediaCodecInfo.getCapabilitiesForType(getMimetype()).getVideoCapabilities().areSizeAndRateSupported(3840, 2160, 30); //UHD1
                    }catch(NullPointerException e){
                        isCapable = false;
                    }

                    context.getSharedPreferences(KEY_PREFS, 0).edit().putInt(KEY_4K_CAPABLE, isCapable ? 1 : 0).apply();
                    return isCapable;
                    //2880, 1920 30 HHD
                }
                else {
                    isCapable = test4KVideo(context);
                    context.getSharedPreferences(KEY_PREFS, 0).edit().putInt(KEY_4K_CAPABLE, isCapable ? 1 : 0).apply();
                    return isCapable;
                }
            }
            return capable == 1;
        }
    }


    public static boolean test4KVideo(Context context){
        //try to extract thumbnail. if it fails, device is not capable.
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try {
            final AssetFileDescriptor afd=context.getAssets().openFd("video4k_test.mp4");
            retriever.setDataSource(afd.getFileDescriptor(),afd.getStartOffset(),afd.getLength());

            Bitmap frameAtTime = retriever.getFrameAtTime();

            if (frameAtTime != null){
                retriever.release();
                frameAtTime.recycle();
                return true;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static String getMimetype(){
        return MediaFormat.MIMETYPE_VIDEO_AVC;
    }

    public static MediaCodecInfo selectCodec(String mimeType) {
        int numCodecs = MediaCodecList.getCodecCount();
        for (int i = 0; i < numCodecs; i++) {
            MediaCodecInfo codecInfo = MediaCodecList.getCodecInfoAt(i);

            if (!codecInfo.isEncoder()) {
                continue;
            }

            String[] types = codecInfo.getSupportedTypes();
            for (int j = 0; j < types.length; j++) {
                if (types[j].equalsIgnoreCase(mimeType)) {
                    return codecInfo;
                }
            }
        }
        return null;
    }
}
