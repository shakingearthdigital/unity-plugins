package com.shakingearthdigital.contentdownloadplugin.utils;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;

import java.util.Arrays;

/**
 * Created by jacob on 9/15/2016.
 */
public class BaseUtil {

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     */
    public static float dpToPx(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     */
    public static float pxToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    public static String ArrayToString(String[] strings) {
        return Arrays.toString(strings);
    }

    public static String[] StringToArray(String string) {
        return string.split(",");
    }
}
