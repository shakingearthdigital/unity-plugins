package com.shakingearthdigital.contentdownloadplugin.utils;

import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.util.Log;
import android.webkit.URLUtil;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.shakingearthdigital.contentdownloadplugin.models.within.Camera;
import com.shakingearthdigital.contentdownloadplugin.models.within.Content;
import com.shakingearthdigital.contentdownloadplugin.models.within.ContentImage;
import com.shakingearthdigital.contentdownloadplugin.models.within.ContentLink;
import com.shakingearthdigital.contentdownloadplugin.models.within.ContentLocal;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by jacob on 9/8/2016.
 */
public class ContentUtil {
    static final com.shakingearthdigital.contentdownloadplugin.utils.SELogUtil log = new com.shakingearthdigital.contentdownloadplugin.utils.SELogUtil(ContentUtil.class.getSimpleName());
    public static String KEY_CONTENT_LOCAL = "contentLocalId";
    public static String KEY_CONTENT_LINK = "contentLinkId";
    Context mContext;

    public static ContentUtil with(Context context){
        ContentUtil c= new ContentUtil();
        c.mContext = context;
        return c;
    }

    public static File getContentFileFromUrl(File folder, String url) {
        return new File(folder, getFileNameFromURL(url));
    }

    public static String getFileNameFromURL(String url) {
        String fileNameWithExtension = "";
        if (URLUtil.isValidUrl(url)) {
            fileNameWithExtension = URLUtil.guessFileName(url, null, null);
            if (fileNameWithExtension != null ) {
                return fileNameWithExtension;
            }
        }
        return fileNameWithExtension;
    }

    public static long getContentSize(String url) {
        if (url == null) return -1;

        OkHttpClient httpClient = new OkHttpClient();
        Call call = httpClient.newCall(new Request.Builder().url(url).get().build());

        try {
            Response response = call.execute();
            if (response.code() == 200) {
                return response.body().contentLength();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }


    public boolean writeContentLocal(Context context, ContentLocal contentLocal, String path, boolean saveToExternalStorage) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            String fullPath = contentLocal.getPath();
            contentLocal.setPath(new File(contentLocal.getPath()).getName());
            File json;
            if(saveToExternalStorage){
                json = com.shakingearthdigital.contentdownloadplugin.managers.ContentManager.getContentFileExternalSD(context, contentLocal.getId());
            }
            else {
                json = com.shakingearthdigital.contentdownloadplugin.managers.ContentManager.getContentFile(context, contentLocal.getId());
            }
            json.delete();
            mapper.writeValue(json, contentLocal);
            MediaScannerConnection.scanFile(mContext, new String[]{json.getAbsolutePath()}, null, null);
            contentLocal.setPath(fullPath);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static ContentLocal createContentLocal(Context context, ContentLink contentLink, String path) {
        ContentLocal local = new ContentLocal();

        local.setId(contentLink.getId());
        local.setTitle(contentLink.getTitle());
        local.setAuthor(contentLink.getAuthor());
        local.setDescription(contentLink.getDescription());
        if (contentLink.getEventId() != null) local.setEventId(contentLink.getEventId());

        // add images
        for (ContentImage image : contentLink.getImages()) {

            Log.d("contentLink.getImages", String.valueOf(contentLink.getImages().size()));
            Log.d("contentLink.getImages", contentLink.getImages().toString());

            local.getImages().add(new ContentImage(image.getType(), new File(image.getLink()).getName()));
        }

        local.setPath(path);
        local.setPlayback(contentLink.getPlayback());
        local.setContentWarning(contentLink.getContentWarning());
        local.setCameras(new ArrayList<Camera>());

        if(contentLink.getAudioSampleRate() == 0) local.setAudioSampleRate(44100);//default to 44.1kHz
        else local.setAudioSampleRate(contentLink.getAudioSampleRate());

        for (Camera camera : contentLink.getCameras()) {
            Camera newCam = new Camera();
            newCam.setVideo(getFileNameFromURL(camera.getVideo()));
            newCam.setVideoFormat(camera.getVideoFormat());

            String[] audio = camera.getAudio();
            if (audio != null) {
                String[] newAudio = new String[audio.length];
                for (int i = 0; i < audio.length; i++) {
                    newAudio[i] = getFileNameFromURL(audio[i]);
                }
                newCam.setAudio(newAudio);
            }
            local.getCameras().add(newCam);
        }

        return local;
    }

    public static String getContentKey(Content content){
        if(content instanceof ContentLocal){
            return KEY_CONTENT_LOCAL;
        }
        else if(content instanceof ContentLink){
            return KEY_CONTENT_LINK;
        }
        return null;
    }

    public static String getKey(Intent intent){
        if(intent.getSerializableExtra(KEY_CONTENT_LINK) != null){
            return KEY_CONTENT_LINK;
        }
        else{
            return KEY_CONTENT_LOCAL;
        }
    }

    public static Content getContent(Intent intent){
        return (Content) intent.getSerializableExtra(ContentUtil.getKey(intent));
    }

    public static Content getContent(Intent intent, String key){
        return (Content) intent.getSerializableExtra(key);
    }

}
